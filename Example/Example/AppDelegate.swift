//
//  AppDelegate.swift
//  Example
//
//  Created by Andre Alves on 3/16/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import UIKit
import Faster

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FasterLogger.shared.currentLevel = .info
        Faster.start(appKey: "9e1bb26f-1815-4bc2-8a0e-535cf8d82781", secretKey: "QMkklih7sZRQhodeCJv9g5m9")

        // Register with APNs
        UIApplication.shared.registerForRemoteNotifications()
        return true
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Faster.updateRemoteNotifications(token: deviceToken)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error as NSError)
    }
}
