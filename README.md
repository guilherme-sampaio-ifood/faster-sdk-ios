![FASTER](https://bitbucket.org/Movile/faster-sdk-ios/raw/a7bc278124a22fb0200e2ff590d97b7d419da527/Docs/faster_logo.jpg)

Faster is an analytics framework with control of installations, sessions and important events.

In this repository you will find all the code related to the project's **core**:

- Identification of devices for control of installations, reinstallations and updates;
- Session control;
- Analytics for important events;
- Storage;
- Network layer.

The core has all the necessary structure for future modules to be developed on top of it.


## Requirements

- iOS 8.0+
- Swift 3.2+


## Installation

### CocoaPods

To integrate Faster into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
use_frameworks!

target '<Your Target Name>' do
    pod 'Faster', :git => 'https://bitbucket.com/Movile/faster-sdk-ios.git', :tag => '0.0.3'
end
```

### Carthage

To integrate Faster into your Xcode project using Carthage, specify it in your `Cartfile`:

```ogdl
git "https://bitbucket.com/Movile/faster-sdk-ios" ~> 0.0
```

Run `carthage update` to build the framework and drag the built `Faster.framework` into your Xcode project.


### Manually

Copy all the files from the folder **Core** to your project.
Check the file **Faster.h** for the list of Objective-C files you are going to need to include in your bridging header. 


## Usage

*All public API is in file **Faster.swift***

The first step is to start the SDK in your **AppDelegate**.

The second (optional) step is to register the user's push token (if your app support remote notifications):

```swift
import Faster

func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Step 1 - Start Faster
    Faster.start(appKey: "your application key", secretKey: "your secret key")

    // Step 2 - Register with APNs
    UIApplication.shared.registerForRemoteNotifications()
    return true
}

func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    Faster.updateRemoteNotifications(token: deviceToken)
}
```


The third (optional) step is to enable iCloud tracking. To do so, add the entitlement *com.apple.developer.ubiquity-kvstore-identifier*.

![Capabilities](https://bitbucket.org/Movile/faster-sdk-ios/raw/a7bc278124a22fb0200e2ff590d97b7d419da527/Docs/cloud-entitlement.png)


*Note: Public APIs can change at any time until the first stable version is released.*


## Contributing

Just clone the project and open the .xcworkspace.

Everyone is welcome to open a pull request and help evolve the project.
