//
//  DeviceDataPersistorTests.swift
//  DeviceDataPersistorTests
//
//  Created by Andre Alves on 3/16/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import XCTest
@testable import Faster

class DeviceDataPersistorTests: XCTestCase {

    func testSaveAndDelete() {
        let persistor = DeviceDataPersistor(applicationKey: UUID().uuidString)

        let persistedData = DeviceData()
        persistor.persist(persistedData)

        switch persistor.retrieve() {
        case .data(let data):
            XCTAssert(data.id == persistedData.id, "id should match")

        case .reinstallation, .notFound:
            XCTAssert(false, "new device data expected")
        }

        persistor.persist(nil)

        switch persistor.retrieve() {
        case .data:
            XCTAssert(false, "device data should should not exist")

        case .reinstallation:
            XCTAssert(false, "device data should should not exist")

        case .notFound:
            XCTAssert(true, "deleted device data")
        }
    }
}
