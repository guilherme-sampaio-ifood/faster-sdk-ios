#
#  Be sure to run `pod spec lint Faster.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "Faster"
  s.version      = "0.0.2"
  s.summary      = "Faster Together"

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  s.description  = <<-DESC
  No description yet.
                   DESC

  s.homepage     = "https://github.com/Movile/faster-sdk-ios"
  
  s.license      = {
    :type => 'Copyright',
    :text => <<-LICENSE
      Copyright 2018 Movile. All rights reserved.
      LICENSE
  }

  s.author       = { "Movile" => "apps@movile.com" }

  #  When using multiple platforms
  s.ios.deployment_target = "8.0"
  #s.osx.deployment_target = '10.11'
  #s.tvos.deployment_target = '9.0'
  #s.watchos.deployment_target = '2.0'

  s.source       = { :git => "https://github.com/Movile/faster-sdk-ios.git", :tag => s.version.to_s }
  s.source_files = 'Sources/**/*.{h,m,swift}'

end
