//
//  Faster.h
//  Faster
//
//  Created by Andre Alves on 3/16/18.
//  Copyright © 2018 Movile. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Faster.
FOUNDATION_EXPORT double FasterVersionNumber;

//! Project version string for Faster.
FOUNDATION_EXPORT const unsigned char FasterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Faster/PublicHeader.h>
#import "FTExceptionHandler.h"
#import "LUKeychainAccess.h" // from https://github.com/TheLevelUp/LUKeychainAccess
#import "FTGZIP.h" // from https://github.com/nicklockwood/GZIP
#import "FTHMAC.h"
