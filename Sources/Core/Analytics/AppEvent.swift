//
//  AppEvent.swift
//  Faster
//
//  Created by Andre Alves on 3/26/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

/// An immutable struct representing recordable events. The event is always linked to the current session at the time of registration.
public struct AppEvent: Codable {
    /// A unique identifier. Should always be a random UUID.
    public let id: String
    /// A unique external identifier, used for deduplication.
    public let externalId: String?
    /// The event's unique name/type.
    public let type: String
    /// The revision number indicates the event's current version. Starts at 0.
    public let revision: Int
    /// The creation timestamp.
    public let creationTimestamp: TimeInterval
    /// Custom properties.
    public let dimensions: [String: DimensionValue]?

    public init(id: String = UUID().uuidString,
                externalId: String?,
                type: String,
                revision: Int = 0,
                creationTimestamp: TimeInterval = Date().timeIntervalSince1970,
                dimensions: [String: DimensionValue]?) {

        self.id = id
        self.externalId = externalId
        self.type = type
        self.revision = revision
        self.creationTimestamp = creationTimestamp
        self.dimensions = dimensions
    }

    public enum CodingKeys: String, CodingKey {
        case id = "eventId"
        case externalId
        case type = "eventType"
        case revision = "eventTypeRevision"
        case creationTimestamp = "localTimestamp"
        case dimensions
    }

    public func asDictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[CodingKeys.id.stringValue] = id
        dictionary[CodingKeys.externalId.stringValue] = externalId
        dictionary[CodingKeys.type.stringValue] = type
        dictionary[CodingKeys.revision.stringValue] = revision
        dictionary[CodingKeys.creationTimestamp.stringValue] = creationTimestamp
        dictionary[CodingKeys.dimensions.stringValue] = dimensions?.mapValues { $0.asAny() }
        return dictionary
    }
}
