//
//  AppEventController.swift
//  Faster
//
//  Created by Andre Alves on 3/27/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class AppEventController {
    internal let buffer: NetworkBuffer

    private var currentSessionForEvents: Session?

    internal init(networkBuffer: NetworkBuffer) {
        self.buffer = networkBuffer
        self.buffer.addHandler(TrackEventRequestHandler())
    }

    internal func setCurrentSessionForEvents(_ session: Session?) {
        self.currentSessionForEvents = session
    }

    internal func sendRequest(forEvent event: AppEvent) {
        guard let session = currentSessionForEvents else {
            return
        }

        let request = TrackEventRequest(deviceId: session.deviceId, sessionId: session.id, event: event)
        buffer.addRequest(request)
    }
}
