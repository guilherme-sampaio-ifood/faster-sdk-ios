//
//  PurchaseEventBuilder.swift
//  Faster
//
//  Created by Andre Alves on 3/28/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

public struct PurchaseEventBuilder {
    public static let eventType = "purchase"
    public static let eventRevision = 0

    public enum DimensionKeys: String {
        case price
        case currency
        case productId
        case quantity
    }

    /**
     Builds a event of type **purchase**.

     - parameter price: The price.
     - parameter currency: The currency.
     - parameter productId: The productId.
     - parameter quantity: The quantity. Default is 1.
     - parameter externalId: A unique external identifier for the event, used for deduplication. Default is nil.
     */
    public static func buildEvent(price: Double, currency: String, productId: String?, quantity: Int = 1, externalId: String? = nil) -> AppEvent {
        var dimensions: [String: DimensionValue] = [:]

        dimensions[DimensionKeys.price.rawValue] = .number(Decimal(price))
        dimensions[DimensionKeys.currency.rawValue] = .string(currency)
        dimensions[DimensionKeys.productId.rawValue] = productId.map(DimensionValue.string)
        dimensions[DimensionKeys.quantity.rawValue] = .number(Decimal(quantity))

        return AppEvent(externalId: externalId, type: eventType, revision: eventRevision, dimensions: dimensions)
    }
}
