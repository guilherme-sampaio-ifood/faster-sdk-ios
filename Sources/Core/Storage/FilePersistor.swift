//
//  FilePersistor.swift
//  Faster
//
//  Created by Andre Alves on 3/21/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class FilePersistor {

    internal static func save<T: Encodable>(object: T, toURL: URL) throws {
        let data = try JSONEncoder().encode(object)
        try FileHelper().saveData(data, destinationURL: toURL)
    }

    internal static func delete(fileURL url: URL) throws {
        try FileHelper().deleteItemAtURLIfExists(url)
    }

    internal static func readObject<T: Decodable>(fromURL url: URL) throws -> T? {
        guard FileHelper().fileExistsAtURL(url) else { return nil }

        let data = try FileHelper().loadDataFromURL(url)
        return try JSONDecoder().decode(T.self, from: data)
    }
}
