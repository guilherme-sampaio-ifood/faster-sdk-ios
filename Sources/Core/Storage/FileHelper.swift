//
//  FileHelper.swift
//  Faster
//
//  Created by Andre Alves on 3/20/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

final class FileHelper {
    enum Errors: Error {
        case unknown
    }

    func deleteItemAtURLIfExists(_ url: URL) throws {
        var error, accessorError: NSError?
        let coordinator = NSFileCoordinator()

        coordinator.coordinate(writingItemAt: url, options: .forDeleting, error: &error) { url in
            let path = url.path
            let manager = FileManager.default

            if manager.fileExists(atPath: path) {
                do {
                    try manager.removeItem(atPath: path)
                } catch let err as NSError {
                    accessorError = err
                }
            }
        }

        if let throwError = error { throw throwError }
        if let throwError = accessorError { throw throwError }
    }

    func createDirIfNeedForFileAtURL(_ url: URL) throws {
        try createDirIfNeedAtURL(url.deletingLastPathComponent())
    }

    func createDirIfNeedAtURL(_ url: URL) throws {
        var error, accessorError: NSError?
        let coordinator = NSFileCoordinator()
        let writingItemAt = url

        coordinator.coordinate(writingItemAt: writingItemAt, options: .forDeleting, error: &error) { destUrl in
            let path = destUrl.path
            let manager = FileManager.default

            if !manager.fileExists(atPath: path) {
                do {
                    try manager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
                } catch let err as NSError {
                    accessorError = err
                }
            }
        }

        if let throwError = error { throw throwError }
        if let throwError = accessorError { throw throwError }
    }

    func moveFileFromURL(_ fromURL: URL, toURL: URL) throws {
        try createDirIfNeedForFileAtURL(toURL)

        if fileExistsAtURL(fromURL) {
            try deleteItemAtURLIfExists(toURL)
        }

        var error, accessorError: NSError?
        let coordinator = NSFileCoordinator()

        let accessor = { (sourceUrl: URL, destUrl: URL) in
            do {
                try FileManager.default.moveItem(atPath: sourceUrl.path, toPath: destUrl.path)
            } catch let err as NSError {
                accessorError = err
            }
        }

        coordinator.coordinate(writingItemAt: fromURL,
                               options: .forMoving,
                               writingItemAt: toURL,
                               options: .forReplacing,
                               error: &error, byAccessor: accessor)

        if let throwError = error { throw throwError }
        if let throwError = accessorError { throw throwError }
    }

    func moveContentsFromDirectoryURL(_ fromDirectoryURL: URL, toDirectoryURL: URL) throws {
        let files = try filesFromDirectoryURL(fromDirectoryURL)

        for file in files {
            guard let fileName = fileNameForFile(file) else { continue }

            let isDirectory = urlIsDirectory(file)

            let destinationURL = toDirectoryURL.appendingPathComponent(fileName, isDirectory: isDirectory)
            try moveFileFromURL(file, toURL: destinationURL)
        }
    }

    func filesFromDirectoryURL(_ directoryURL: URL) throws -> [URL] {
        let fileManager = FileManager.default
        let keys: [URLResourceKey] = [.isDirectoryKey, .creationDateKey]
        return try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: keys, options: [])
    }

    func fileExistsAtURL(_ url: URL) -> Bool {
        var exists = false
        let coordinator = NSFileCoordinator()

        coordinator.coordinate(readingItemAt: url, options: .withoutChanges, error: nil) { url in
            exists = FileManager.default.fileExists(atPath: url.path)
        }

        return exists
    }

    func saveData(_ data: Data, destinationURL: URL) throws {
        try createDirIfNeedForFileAtURL(destinationURL)
        try data.write(to: destinationURL, options: .atomic)
    }

    func loadDataFromURL(_ fromURL: URL) throws -> Data {
        return try Data(contentsOf: fromURL, options: .uncached)
    }

    func fileNameForFile(_ file: URL) -> String? {
        guard !file.lastPathComponent.isEmpty else { return nil }
        return file.lastPathComponent
    }

    func urlIsDirectory(_ url: URL) -> Bool {
        var isDirectory: AnyObject?
        _ = try? (url as NSURL).getResourceValue(&isDirectory, forKey: .isDirectoryKey)
        return (isDirectory as? NSNumber)?.boolValue ?? false
    }

    func creationDateForFile(_ file: URL) -> Date? {
        var creationDate: AnyObject?
        _ = try? (file as NSURL).getResourceValue(&creationDate, forKey: .creationDateKey)
        return (creationDate as? NSDate) as Date?
    }

    func sizeForFile(_ file: URL) throws -> Int64 {
        let fileAttributes = try FileManager.default.attributesOfItem(atPath: file.path)
        return (fileAttributes[.size] as? NSNumber)?.int64Value ?? 0
    }

    func sizeForDirectory(_ directory: URL) throws -> Int64 {
        var error: Error?
        var accumulatedSize: Int64 = 0

        let keys: [URLResourceKey] = [.isRegularFileKey, .fileAllocatedSizeKey, .totalFileAllocatedSizeKey]

        let errorHandler: (URL, Error) -> Bool = { (url, localError) in
            error = localError
            return false
        }

        guard let enumerator = FileManager.default.enumerator(at: directory,
                                                              includingPropertiesForKeys: keys,
                                                              options: [],
                                                              errorHandler: errorHandler) else { throw Errors.unknown }

        for case let fileURL as NSURL in enumerator {
            // Get the type of this item, making sure we only sum up sizes of regular files.
            var isRegularFile = false

            do {
                var object: AnyObject?
                _ = try? fileURL.getResourceValue(&object, forKey: .isRegularFileKey)
                isRegularFile = (object as? NSNumber)?.boolValue ?? false
            }

            guard isRegularFile else { continue } // Ignore anything except regular files.

            var fileSize: Int64 = 0

            // To get the file's size we first try the most comprehensive value in terms of what the file may use on disk.
            // This includes metadata, compression (on file system level) and block size.
            do {
                var object: AnyObject?
                _ = try? fileURL.getResourceValue(&object, forKey: .totalFileAllocatedSizeKey)
                fileSize = (object as? NSNumber)?.int64Value ?? 0
            }

            // In case the value is unavailable we use the fallback value (excluding meta data and compression)
            // This value should always be available.
            if fileSize == 0 {
                do {
                    var object: AnyObject?
                    _ = try? fileURL.getResourceValue(&object, forKey: .fileAllocatedSizeKey)
                    fileSize = (object as? NSNumber)?.int64Value ?? 0
                }
            }

            if fileSize > 0 {
                accumulatedSize += fileSize // We're good, add up the value.
            }
        }

        if let throwError = error { throw throwError }

        return accumulatedSize
    }

    func appendDocumentsPathTo(url: URL) -> URL? {
        guard let documentsPath = documentsDirPath() else { return nil }
        return URL(fileURLWithPath: documentsPath).appendingPathComponent(url.absoluteString)
    }

    func appendCachePathTo(url: URL) -> URL? {
        guard let cachePath = cacheDirPath() else { return nil }
        return URL(fileURLWithPath: cachePath).appendingPathComponent(url.absoluteString)
    }

    func documentsDirPath() -> String? {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
    }

    func cacheDirPath() -> String? {
        return NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first
    }

    func sanitizeString(_ string: String) -> String {
        return string.trimmingCharacters(in: invalidCharacterSet() as CharacterSet)
    }

    func invalidCharacterSet() -> NSMutableCharacterSet {
        let invalidCharacters = NSMutableCharacterSet(charactersIn: ":/ ")
        invalidCharacters.formUnion(with: .newlines)
        invalidCharacters.formUnion(with: .illegalCharacters)
        invalidCharacters.formUnion(with: .controlCharacters)
        return invalidCharacters
    }
}
