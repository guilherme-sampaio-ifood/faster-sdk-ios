//
//  CloudController.swift
//  Faster
//
//  Created by Andre Alves on 3/20/18.
//  Copyright © 2018 Movile. All rights reserved.
//
// https://stackoverflow.com/a/12336783/9316355

// Your app need the entitlement com.apple.developer.ubiquity-kvstore-identifier to use this feature!

import Foundation

internal final class CloudController {
    internal static let shared = CloudController()

    internal static let didChangeExternallyNotification = Notification.Name(rawValue: "CloudControllerDidChangeExternally")

    private let uuidKey = "com.movile.Faster.uuid"

    internal private(set) var isAvailable: Bool = false

    init() {
        let name = NSUbiquitousKeyValueStore.didChangeExternallyNotification
        let object = NSUbiquitousKeyValueStore.default
        NotificationCenter.default.addObserver(self, selector: #selector(storeDidChange), name: name, object: object)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    internal var cloudId: String? {
        set {
            if let value = newValue {
                NSUbiquitousKeyValueStore.default.set(value, forKey: uuidKey)
            } else {
                NSUbiquitousKeyValueStore.default.removeObject(forKey: uuidKey)
            }
            synchronize()
        }
        get {
            return NSUbiquitousKeyValueStore.default.string(forKey: uuidKey)
        }
    }

    internal func synchronize() {
        isAvailable = NSUbiquitousKeyValueStore.default.synchronize()

        if !isAvailable {
            log.info("failed to sync with iCloud, please verify if the app was built with the proper entitlements.")
        }
    }

    @objc private func storeDidChange(notification: Notification) {
        if let changedKeys = notification.userInfo?[NSUbiquitousKeyValueStoreChangedKeysKey] as? [String] {
            if changedKeys.contains(uuidKey) {
                // make sure to post in main because don't know from which thread this notification is called
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: CloudController.didChangeExternallyNotification, object: self)
                }
            }
        }
    }
}
