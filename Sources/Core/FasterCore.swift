//
//  FasterCore.swift
//  Faster
//
//  Created by Andre Alves on 3/20/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class FasterCore {
    internal let applicationKey: String
    internal let configuration: FasterConfiguration

    internal let installer: Installer
    internal let sessionController: SessionController
    internal let buffer: NetworkBuffer

    internal let eventController: AppEventController

    internal init(appKey: String, secretKey: String, configuration: FasterConfiguration) {
        log.info("starting with application key \(appKey)")

        self.applicationKey = appKey
        self.configuration = configuration

        let buffer = NetworkBuffer(applicationKey: appKey, secret: secretKey)
        self.buffer = buffer

        self.installer = Installer(networkBuffer: buffer)
        self.sessionController = SessionController(networkBuffer: buffer, configuration: configuration)
        self.eventController = AppEventController(networkBuffer: buffer)

        self.installer.delegate = self
        self.sessionController.delegate = self

        self.installer.install()
    }

    internal func eraseAllData() {
        buffer.removeAllRequests()
        installer.uninstall()
        sessionController.deleteCurrentSession()
        eventController.setCurrentSessionForEvents(nil)
    }
}

extension FasterCore: InstallerDelegate {

    func installer(_ installer: Installer, didInstall deviceData: DeviceData) {
        log.info("new install detected. Creating DeviceData...")
        log.debug(deviceData)

        sessionController.start(deviceData: deviceData, mode: .install)
    }

    func installer(_ installer: Installer, didReinstall deviceData: DeviceData) {
        log.info("reinstallation detected, creating new DeviceData based on deviceId: \(deviceData.id)")
        log.debug(deviceData)

        sessionController.start(deviceData: deviceData, mode: .reinstall)
    }

    func installer(_ installer: Installer, didOpen deviceData: DeviceData) {
        log.info("simple open detected, using persisted DeviceData with id \(deviceData.id)")
        log.debug(deviceData)

        sessionController.start(deviceData: deviceData, mode: .open)
    }

    func installer(_ installer: Installer, didUpdate deviceData: DeviceData) {
        log.info("updated DeviceData with id \(deviceData.id)")
        log.debug(deviceData)
    }
}

extension FasterCore: SessionControllerDelegate {

    func controller(_ controller: SessionController, didOpenSession session: Session) {
        log.info("did open session \(session.id)")
        log.debug(session)

        eventController.setCurrentSessionForEvents(session)
    }

    func controller(_ controller: SessionController, didCloseSession session: Session) {
        log.info("did close session \(session.id)")
        log.debug(session)

        eventController.setCurrentSessionForEvents(nil)
    }

    func controller(_ controller: SessionController, didUpdateSession session: Session) {
        log.info("did update session \(session.id)")
        log.debug(session)
    }
}
