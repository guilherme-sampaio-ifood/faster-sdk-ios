//
//  SessionLifePersistor.swift
//  Faster
//
//  Created by Andre Alves on 3/23/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class SessionLifePersistor {
    private let documentsPath: String

    internal init(applicationKey: String) {
        let appKey = FileHelper().sanitizeString(applicationKey)
        self.documentsPath = "Faster/\(appKey)/Sessions/Pings/"
    }

    internal func persistLastPingDate(_ date: Date?, forSession session: Session) {
        persistInDocuments(session, lastPingDate: date)
    }

    internal func retrieveLastPingDate(forSession session: Session) -> Date? {
        return retrieveFromDocuments(session)
    }

    // MARK: Documents
    private func persistInDocuments(_ session: Session, lastPingDate: Date?) {
        guard let fileURL = documentsURLForSession(session) else { return }

        do {
            if let date = lastPingDate {
                let data = NSKeyedArchiver.archivedData(withRootObject: date)
                try FileHelper().saveData(data, destinationURL: fileURL)
            } else {
                try FileHelper().deleteItemAtURLIfExists(fileURL)
            }
        } catch let error as NSError {
            log.error("failed to persist ping date for session \(session.id) in Documents with error: \(error)")
        }
    }

    private func retrieveFromDocuments(_ session: Session) -> Date? {
        guard let fileURL = documentsURLForSession(session) else { return nil }
        guard FileHelper().fileExistsAtURL(fileURL) else { return nil }

        do {
            let data = try FileHelper().loadDataFromURL(fileURL)
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? Date
        } catch let error as NSError {
            log.error("failed to read ping date for session \(session.id) in Documents with error: \(error)")
            return nil
        }
    }

    private func documentsURLForSession(_ session: Session) -> URL? {
        guard let documentsDir = FileHelper().documentsDirPath() else { return nil }
        let datesPath = (documentsDir as NSString).appendingPathComponent(documentsPath)
        let filePath = (datesPath as NSString).appendingPathComponent(session.id + ".dat")
        return URL(fileURLWithPath: filePath)
    }
}
