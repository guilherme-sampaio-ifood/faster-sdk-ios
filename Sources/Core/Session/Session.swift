//
//  Session.swift
//  Faster
//
//  Created by Andre Alves on 3/16/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

public struct Session: Codable, Equatable {

    public let id: String
    public private(set) var deviceId: String
    public private(set) var userId: String?
    public private(set) var geolocation: Geolocation?
    public private(set) var creationTimestamp: TimeInterval
    public private(set) var closeTimestamp: TimeInterval?
    public private(set) var properties: [String: DimensionValue]

    public enum CodingKeys: String, CodingKey {
        case id = "sessionId"
        case deviceId
        case userId
        case geolocation = "geoPoint"
        case creationTimestamp = "localTimestamp"
        case closeTimestamp
        case properties = "sessionProperties"
    }

    public struct Geolocation: Codable, Equatable {
        public var latitude: Double
        public var longitude: Double

        public init(latitude: Double, longitude: Double) {
            self.latitude = latitude
            self.longitude = longitude
        }

        public func asDictionary() -> [String: Any] {
            var dictionary: [String: Any] = [:]
            dictionary[CodingKeys.latitude.stringValue] = latitude
            dictionary[CodingKeys.longitude.stringValue] = longitude
            return dictionary
        }

        public static func ==(lhs: Session.Geolocation, rhs: Session.Geolocation) -> Bool {
            return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
        }
    }

    internal mutating func update(userId: String?) {
        self.userId = userId
    }

    internal mutating func update(geolocation: Geolocation?) {
        self.geolocation = geolocation
    }

    internal mutating func update(closeDate: Date?) {
        self.closeTimestamp = closeDate?.timeIntervalSince1970
    }

    internal mutating func updateProperties(value: DimensionValue?, forKey key: String) {
        self.properties[key] = value
    }

    internal init(deviceData: DeviceData) {
        self.id = UUID().uuidString
        self.deviceId = deviceData.id
        self.creationTimestamp = Date().timeIntervalSince1970
        self.properties = [:]
    }

    public func asDictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[CodingKeys.id.stringValue] = id
        dictionary[CodingKeys.deviceId.stringValue] = deviceId
        dictionary[CodingKeys.userId.stringValue] = userId
        dictionary[CodingKeys.geolocation.stringValue] = geolocation?.asDictionary()
        dictionary[CodingKeys.creationTimestamp.stringValue] = creationTimestamp
        dictionary[CodingKeys.closeTimestamp.stringValue] = closeTimestamp
        dictionary[CodingKeys.properties.stringValue] = properties.mapValues { $0.asAny() }
        return dictionary
    }

    public static func ==(lhs: Session, rhs: Session) -> Bool {
        return lhs.id == rhs.id && lhs.deviceId == rhs.deviceId && lhs.userId == rhs.userId &&
            lhs.geolocation == rhs.geolocation && lhs.creationTimestamp == rhs.creationTimestamp &&
            lhs.closeTimestamp == rhs.closeTimestamp && lhs.properties == rhs.properties
    }
}
