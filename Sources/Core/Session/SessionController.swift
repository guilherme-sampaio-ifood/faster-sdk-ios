//
//  SessionController.swift
//  Faster
//
//  Created by Andre Alves on 3/20/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal protocol SessionControllerDelegate: class {
    func controller(_ controller: SessionController, didOpenSession session: Session)
    func controller(_ controller: SessionController, didCloseSession session: Session)
    func controller(_ controller: SessionController, didUpdateSession session: Session)
}

internal final class SessionController {
    internal weak var delegate: SessionControllerDelegate?

    internal let buffer: NetworkBuffer
    private let trackInstall: Bool
    private let persistor: SessionPersistor
    private let lifeController: SessionLifeController

    private(set) var session: Session?
    private var deviceData: DeviceData?

    private var userId: PropertySet<String?>?
    private var geolocation: PropertySet<Session.Geolocation?>?
    private var properties: [String: PropertySet<DimensionValue?>] = [:]

    internal init(networkBuffer: NetworkBuffer, configuration: FasterConfiguration) {
        self.buffer = networkBuffer
        self.trackInstall = configuration.trackInstall
        self.persistor = SessionPersistor(applicationKey: networkBuffer.applicationKey)
        self.lifeController = SessionLifeController(applicationKey: networkBuffer.applicationKey,
                                                    sessionTimeout: configuration.sessionTimeout)
        self.lifeController.delegate = self

        self.buffer.addHandler(CreateDeviceRequestHandler())
        self.buffer.addHandler(CreateSessionRequestHandler())
        self.buffer.addHandler(UpdateSessionRequestHandler())
        self.buffer.addHandler(CloseSessionRequestHandler())
    }

    deinit {
        lifeController.invalidateTimer()
    }

    internal enum StartMode {
        case install, reinstall, open
    }

    internal func start(deviceData: DeviceData, mode: StartMode) {
        self.deviceData = deviceData

        var oldSession = persistor.retrieve()
        let oldSessionExpired = oldSession.flatMap(lifeController.isSessionExpired) ?? false
        let deviceDataChanged = oldSession?.deviceId != deviceData.id

        if let session = oldSession, oldSessionExpired || deviceDataChanged {
            closeSession(session, closeDate: oldSession.flatMap(lifeController.lastPingDate))
            oldSession = nil
        }

        switch mode {
        case .install, .reinstall:
            let newSession = createNewSession(deviceData: deviceData)
            self.session = newSession
            sendInstallEvent(session: newSession, deviceData: deviceData, isReinstall: mode == .reinstall)
            delegate?.controller(self, didOpenSession: newSession)

        case .open:
            if let session = oldSession {
                self.session = session
                delegate?.controller(self, didOpenSession: session)
                refreshCurrentSession(deviceData: deviceData)

            } else {
                let newSession = createNewSession(deviceData: deviceData)
                self.session = newSession
                sendSessionOpenEvent(session: newSession)
                delegate?.controller(self, didOpenSession: newSession)
            }
        }

        lifeController.monitoringSession = session
    }

    internal func updateSession(geolocation: Session.Geolocation?) {
        self.geolocation = PropertySet(geolocation)

        if let deviceData = deviceData, let session = session, session.geolocation != geolocation {
            refreshCurrentSession(deviceData: deviceData)
        }
    }

    internal func updateSession(userId: String?) {
        self.userId = PropertySet(userId)

        if let deviceData = deviceData, let session = session, session.userId != userId {
            refreshCurrentSession(deviceData: deviceData)
        }
    }

    internal func updateSessionProperties(value: DimensionValue?, forKey key: String) {
        self.properties[key] = PropertySet(value)

        if let deviceData = deviceData, let session = session, session.properties[key] != value {
            refreshCurrentSession(deviceData: deviceData)
        }
    }

    internal func deleteCurrentSession() {
        session = nil
        deviceData = nil
        lifeController.monitoringSession.map(lifeController.eraseAuxiliaryData)
        lifeController.monitoringSession = nil
        persistor.persist(nil)
    }

    private func refreshCurrentSession(deviceData: DeviceData) {
        guard let currentSession = session else { return }

        let updatedSession = updateSessionWithCurrentProperties(currentSession)

        if updatedSession != session {
            session = updatedSession
            session.map(sendSessionUpdateEvent)
            persistor.persist(updatedSession)
            delegate?.controller(self, didUpdateSession: updatedSession)
        }
    }

    private func createNewSession(deviceData: DeviceData) -> Session {
        var session = Session(deviceData: deviceData)
        session = updateSessionWithCurrentProperties(session)
        persistor.persist(session)
        return session
    }

    private func updateSessionWithCurrentProperties(_ session: Session) -> Session {
        var updatedSession = session

        if let userId = self.userId {
            updatedSession.update(userId: userId.value)
        }
        if let geolocation = self.geolocation {
            updatedSession.update(geolocation: geolocation.value)
        }
        for (key, property) in self.properties {
            updatedSession.updateProperties(value: property.value, forKey: key)
        }

        return updatedSession
    }

    private func closeSession(_ session: Session, closeDate: Date?) {
        var closedSession = session
        closedSession.update(closeDate: closeDate)

        if self.session?.id == closedSession.id {
            self.session = nil
        }

        sendSessionCloseEvent(session: closedSession)
        delegate?.controller(self, didCloseSession: closedSession)
        lifeController.eraseAuxiliaryData(session: closedSession)
        persistor.persist(nil)
    }

    private func sendInstallEvent(session: Session, deviceData: DeviceData, isReinstall: Bool) {
        let request = CreateDeviceRequest(deviceData: deviceData, session: session,
                                          isReinstall: isReinstall,
                                          trackInstall: trackInstall || isReinstall)
        buffer.addRequest(request)
    }

    private func sendSessionOpenEvent(session: Session) {
        buffer.addRequest(CreateSessionRequest(session: session))
    }

    private func sendSessionCloseEvent(session: Session) {
        buffer.addRequest(CloseSessionRequest(session: session))
    }

    private func sendSessionUpdateEvent(session: Session) {
        buffer.addRequest(UpdateSessionRequest(session: session))
    }
}

extension SessionController: SessionLifeControllerDelegate {
    func lifeController(_ lifeController: SessionLifeController, sessionDidExpire session: Session, date: Date) {
        closeSession(session, closeDate: date)

        if let deviceData = self.deviceData {
            start(deviceData: deviceData, mode: .open)
        }
    }
}

private struct PropertySet<T> {
    var value: T
    init(_ value: T) { self.value = value }
}
