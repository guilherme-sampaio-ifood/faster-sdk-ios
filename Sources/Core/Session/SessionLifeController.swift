//
//  SessionLifeController.swift
//  Faster
//
//  Created by Andre Alves on 3/21/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal protocol SessionLifeControllerDelegate: class {
    func lifeController(_ lifeController: SessionLifeController, sessionDidExpire session: Session, date: Date)
}

internal final class SessionLifeController: NSObject {
    private let sessionTimeout: TimeInterval

    internal weak var delegate: SessionLifeControllerDelegate?

    internal var monitoringSession: Session? {
        didSet {
            if let session = monitoringSession {
                updateLastPingDate(session: session)
            }
        }
    }

    private let persistor: SessionLifePersistor
    private var timer: Timer?
    private var pingDateCache: [String: Date] = [:]

    internal init(applicationKey: String, sessionTimeout: Int) {
        self.persistor = SessionLifePersistor(applicationKey: applicationKey)
        self.sessionTimeout = TimeInterval(sessionTimeout) // 5 minutes by default
        super.init()

        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(applicationWillEnterForeground), name: .UIApplicationWillEnterForeground, object: nil)
        center.addObserver(self, selector: #selector(applicationDidEnterBackground), name: .UIApplicationDidEnterBackground, object: nil)

        // timers can only be scheduled in the main queue
        DispatchQueue.main.async {
            self.scheduleTimer()
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    private func scheduleTimer() {
        guard timer == nil else { return }
        timer = Timer.scheduledTimer(timeInterval: 15,
                                     target: self,
                                     selector: #selector(timerTick),
                                     userInfo: nil,
                                     repeats: true)
    }

    // always remember to call this when finishes using this object
    internal func invalidateTimer() {
        timer?.invalidate()
        timer = nil
    }

    internal func isSessionExpired(session: Session) -> Bool {
        return lastPingDate(session: session).map(isLastPingDateExpired) ?? false
    }

    internal func isLastPingDateExpired(date: Date) -> Bool {
        return abs(Date().timeIntervalSince(date)) >= sessionTimeout
    }

    internal func lastPingDate(session: Session) -> Date? {
        if let date = pingDateCache[session.id] {
            return date
        } else {
            let date = persistor.retrieveLastPingDate(forSession: session)
            pingDateCache[session.id] = date
            return date
        }
    }

    internal func updateLastPingDate(session: Session) {
        let now = Date()
        pingDateCache[session.id] = now
        persistor.persistLastPingDate(now, forSession: session)
    }

    internal func eraseAuxiliaryData(session: Session) {
        pingDateCache[session.id] = nil
        persistor.persistLastPingDate(nil, forSession: session)
    }

    @objc private func timerTick() {
        guard UIApplication.shared.applicationState == .active else { return }

        if let session = monitoringSession {
            updateLastPingDate(session: session)
        }
    }

    @objc private func applicationWillEnterForeground() {
        scheduleTimer()

        guard let session = monitoringSession else { return }

        if let date = lastPingDate(session: session), isLastPingDateExpired(date: date) {
            delegate?.lifeController(self, sessionDidExpire: session, date: date)
        } else {
            updateLastPingDate(session: session)
        }
    }

    @objc private func applicationDidEnterBackground() {
        invalidateTimer()

        if let session = monitoringSession {
            updateLastPingDate(session: session)
        }
    }
}
