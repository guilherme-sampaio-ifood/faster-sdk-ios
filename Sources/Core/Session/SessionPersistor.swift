//
//  SessionPersistor.swift
//  Faster
//
//  Created by Andre Alves on 3/16/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class SessionPersistor {
    private let documentsPath: String

    internal init(applicationKey: String) {
        let appKey = FileHelper().sanitizeString(applicationKey)
        self.documentsPath = "Faster/\(appKey)/Sessions/session.json"
    }

    internal func persist(_ session: Session?) {
        persistInDocuments(session)
    }

    internal func retrieve() -> Session? {
        return retrieveFromDocuments()
    }

    // MARK: Documents
    private func persistInDocuments(_ session: Session?) {
        guard let fileURL = documentsURLForSession() else { return }

        do {
            if let session = session {
                try FilePersistor.save(object: session, toURL: fileURL)
            } else {
                try FilePersistor.delete(fileURL: fileURL)
            }
        } catch let error as NSError {
            log.error("failed to persist session in Documents with error: \(error)")
        }
    }

    private func retrieveFromDocuments() -> Session? {
        guard let fileURL = documentsURLForSession() else { return nil }

        do {
            return try FilePersistor.readObject(fromURL: fileURL)
        } catch let error as NSError {
            log.error("failed to read session from Documents with error: \(error)")
            return nil
        }
    }

    private func documentsURLForSession() -> URL? {
        guard let documentsDir = FileHelper().documentsDirPath() else { return nil }
        let filePath = (documentsDir as NSString).appendingPathComponent(documentsPath)
        return URL(fileURLWithPath: filePath)
    }
}
