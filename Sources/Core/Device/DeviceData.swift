//
//  DeviceData.swift
//  Faster
//
//  Created by Andre Alves on 3/16/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import UIKit
import AdSupport
import CoreTelephony

public struct DeviceData: Codable, Equatable {

    public let id: String
    public private(set) var vendorId: String? // ifv
    public private(set) var advertisingId: String? // ifa
    public private(set) var cloudId: String?
    public let platform: String = "IOS"
    public private(set) var system: String
    public private(set) var systemVersion: String
    public let manufacturer: String = "Apple"
    public private(set) var pushToken: String?
    public private(set) var deviceModel: String?
    public private(set) var appVersion: String
    public private(set) var sdkVersion: String
    public private(set) var carrierId: String?
    public private(set) var timezone: String
    public private(set) var language: String
    public private(set) var creationTimestamp: TimeInterval
    public private(set) var properties: [String: DimensionValue]

    public enum CodingKeys: String, CodingKey {
        case id = "deviceId"
        case vendorId
        case advertisingId
        case cloudId
        case platform
        case system
        case systemVersion
        case manufacturer
        case pushToken
        case deviceModel
        case appVersion
        case sdkVersion
        case carrierId
        case timezone
        case language
        case properties = "deviceProperties"
        case creationTimestamp = "localTimestamp"
    }

    internal init(id: String = UUID().uuidString) {
        self.id = id
        self.creationTimestamp = Date().timeIntervalSince1970
        self.system = ""
        self.systemVersion = ""
        self.appVersion = ""
        self.sdkVersion = ""
        self.language = ""
        self.timezone = ""
        self.properties = [:]
        refresh()
    }

    internal func refreshed() -> DeviceData {
        var copy = self
        copy.refresh()
        return copy
    }

    internal mutating func refresh() {
        vendorId = UIDevice.current.identifierForVendor?.uuidString

        if ASIdentifierManager.shared().isAdvertisingTrackingEnabled {
            advertisingId = ASIdentifierManager.shared().advertisingIdentifier.uuidString
        }

        if CloudController.shared.isAvailable {
            cloudId = CloudController.shared.cloudId
        }

        system = UIDevice.current.systemName.uppercased()
        systemVersion = UIDevice.current.systemVersion
        deviceModel = devicePlatform()
        appVersion = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? ""
        sdkVersion = (Bundle(for: Faster.self).infoDictionary?["CFBundleShortVersionString"] as? String) ?? ""
        carrierId = CTTelephonyNetworkInfo().subscriberCellularProvider?.carrierName?.uppercased()
        language = NSLocale.preferredLanguages.first ?? ""
        timezone = String(NSTimeZone.local.secondsFromGMT() / 3600)
    }

    internal mutating func update(pushToken: Data?) {
        let tokenParts = pushToken?.map { data -> String in
            return String(format: "%02.2hhx", data)
        }

        self.pushToken = tokenParts?.joined()
    }

    internal mutating func updateProperties(value: DimensionValue?, forKey key: String) {
        self.properties[key] = value
    }

    public func asDictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[CodingKeys.id.stringValue] = id
        dictionary[CodingKeys.vendorId.stringValue] = vendorId
        dictionary[CodingKeys.advertisingId.stringValue] = advertisingId
        dictionary[CodingKeys.cloudId.stringValue] = cloudId
        dictionary[CodingKeys.platform.stringValue] = platform
        dictionary[CodingKeys.system.stringValue] = system
        dictionary[CodingKeys.systemVersion.stringValue] = systemVersion
        dictionary[CodingKeys.manufacturer.stringValue] = manufacturer
        dictionary[CodingKeys.pushToken.stringValue] = pushToken
        dictionary[CodingKeys.deviceModel.stringValue] = deviceModel
        dictionary[CodingKeys.appVersion.stringValue] = appVersion
        dictionary[CodingKeys.sdkVersion.stringValue] = sdkVersion
        dictionary[CodingKeys.carrierId.stringValue] = carrierId
        dictionary[CodingKeys.timezone.stringValue] = timezone
        dictionary[CodingKeys.language.stringValue] = language
        dictionary[CodingKeys.creationTimestamp.stringValue] = creationTimestamp
        dictionary[CodingKeys.properties.stringValue] = properties.mapValues { $0.asAny() }
        return dictionary
    }

    public static func ==(lhs: DeviceData, rhs: DeviceData) -> Bool {
        return lhs.id == rhs.id && lhs.vendorId == rhs.vendorId && lhs.advertisingId == rhs.advertisingId &&
            lhs.cloudId == rhs.cloudId && lhs.platform == rhs.platform && lhs.system == rhs.system &&
            lhs.systemVersion == rhs.systemVersion && lhs.manufacturer == rhs.manufacturer && lhs.pushToken == rhs.pushToken &&
            lhs.deviceModel == rhs.deviceModel && lhs.appVersion == rhs.appVersion && lhs.sdkVersion == rhs.sdkVersion &&
            lhs.carrierId == rhs.carrierId && lhs.timezone == rhs.timezone && lhs.language == rhs.language &&
            lhs.creationTimestamp == rhs.creationTimestamp && lhs.properties == rhs.properties
    }
}

private func devicePlatform() -> String {
    var systemInfo = utsname()
    uname(&systemInfo)
    let machineMirror = Mirror(reflecting: systemInfo.machine)
    let identifier = machineMirror.children.reduce("") { identifier, element in
        guard let value = element.value as? Int8, value != 0 else { return identifier }
        return identifier + String(UnicodeScalar(UInt8(value)))
    }
    return identifier
}
