//
//  Installer.swift
//  Faster
//
//  Created by Andre Alves on 3/20/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal protocol InstallerDelegate: class {
    func installer(_ installer: Installer, didInstall deviceData: DeviceData)
    func installer(_ installer: Installer, didReinstall deviceData: DeviceData)
    func installer(_ installer: Installer, didOpen deviceData: DeviceData)
    func installer(_ installer: Installer, didUpdate deviceData: DeviceData)
}

internal final class Installer {
    internal weak var delegate: InstallerDelegate?

    let buffer: NetworkBuffer
    private let persistor: DeviceDataPersistor
    private(set) var deviceData: DeviceData?

    internal init(networkBuffer: NetworkBuffer) {
        self.buffer = networkBuffer
        self.persistor = DeviceDataPersistor(applicationKey: networkBuffer.applicationKey)

        self.buffer.addHandler(UpdateDeviceRequestHandler())

        self.prepareCloudSync()
    }

    private func prepareCloudSync() {
        // listen for iCloud uuid changes
        let notification = CloudController.didChangeExternallyNotification
        NotificationCenter.default.addObserver(self, selector: #selector(cloudIdChanged), name: notification, object: nil)

        // get changes that might have happened while this instance wasn't running
        CloudController.shared.synchronize()

        if CloudController.shared.isAvailable {
            if CloudController.shared.cloudId == nil {
                CloudController.shared.cloudId = UUID().uuidString
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    internal func install() {
        let result = persistor.retrieve()
        switch result {
        case .data(let persistedDeviceData):
            deviceData = persistedDeviceData
            let updatedData = persistedDeviceData.refreshed()
            if updatedData != persistedDeviceData {
                updateCurrentDeviceData(withData: updatedData)
            }
            delegate?.installer(self, didOpen: updatedData)

        case .reinstallation(let deviceId):
            let newDeviceData = DeviceData(id: deviceId)
            deviceData = newDeviceData
            persistor.persist(newDeviceData)
            delegate?.installer(self, didReinstall: newDeviceData)

        case .notFound:
            let newDeviceData = DeviceData()
            deviceData = newDeviceData
            persistor.persist(newDeviceData)
            delegate?.installer(self, didInstall: newDeviceData)
        }
    }

    internal func uninstall() {
        deviceData = nil
        persistor.persist(nil)

        if CloudController.shared.isAvailable {
            CloudController.shared.cloudId = nil
        }
    }

    internal func updateRemoteNotifications(token: Data?) {
        guard var updatedData = deviceData else {
            return log.warning("device data does not exist") // should never happen...
        }

        updatedData.update(pushToken: token)
        if updatedData != deviceData {
            updateCurrentDeviceData(withData: updatedData)
        }
    }

    internal func updateDeviceProperties(value: DimensionValue?, forKey key: String) {
        guard var updatedData = deviceData else {
            return log.warning("device data does not exist") // should never happen...
        }

        if updatedData.properties[key] != value {
            updatedData.updateProperties(value: value, forKey: key)
            updateCurrentDeviceData(withData: updatedData)
        }
    }

    @objc private func cloudIdChanged() {
        if var updatedData = deviceData {
            updatedData.refresh()

            if updatedData != deviceData {
                log.info("DeviceData's cloudId changed!")
                updateCurrentDeviceData(withData: updatedData)
            }
        }
    }

    private func updateCurrentDeviceData(withData updatedData: DeviceData) {
        deviceData = updatedData
        persistor.persist(updatedData)
        log.debug(updatedData)
        sendDeviceUpdateEvent(deviceData: updatedData)
        delegate?.installer(self, didUpdate: updatedData)
    }

    private func sendDeviceUpdateEvent(deviceData: DeviceData) {
        let request = UpdateDeviceRequest(deviceData: deviceData)
        buffer.addRequest(request)
    }
}
