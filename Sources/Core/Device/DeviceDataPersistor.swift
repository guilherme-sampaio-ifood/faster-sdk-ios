//
//  DeviceDataPersistor.swift
//  Faster
//
//  Created by Andre Alves on 3/20/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class DeviceDataPersistor {
    enum Result {
        case data(DeviceData)
        case reinstallation(deviceId: String)
        case notFound
    }

    private let documentsPath: String
    private let deviceIdKey: String

    internal init(applicationKey: String) {
        let appKey = FileHelper().sanitizeString(applicationKey)

        self.documentsPath = "Faster/\(appKey)/Devices/data.json"
        self.deviceIdKey = "com.movile.faster.deviceId." + appKey

        LUKeychainAccess.standard().accessibilityState = .attrAccessibleAlways
    }

    internal func persist(_ deviceData: DeviceData?) {
        persistInDocuments(deviceData)
        persistDeviceIdInKeychain(deviceData?.id)
        persistDeviceIdInPasteboard(deviceData?.id)
    }

    internal func retrieve() -> Result {
        if let deviceData = retrieveFromDocuments() {
            return .data(deviceData)

        } else if let id = retrieveDeviceIdFromKeychain() {
            return .reinstallation(deviceId: id)

        } else if let id = retrieveDeviceIdFromPasteboard() {
            return .reinstallation(deviceId: id)

        } else {
            return .notFound
        }
    }

    // MARK: Documents
    private func persistInDocuments(_ deviceData: DeviceData?) {
        guard let fileURL = documentsURLForDeviceData() else { return }

        do {
            if let data = deviceData {
                try FilePersistor.save(object: data, toURL: fileURL)
            } else {
                try FilePersistor.delete(fileURL: fileURL)
            }
        } catch let error as NSError {
            log.error("failed to persist DeviceData in Documents with error: \(error)")
        }
    }

    private func retrieveFromDocuments() -> DeviceData? {
        guard let fileURL = documentsURLForDeviceData() else { return nil }

        do {
            return try FilePersistor.readObject(fromURL: fileURL)
        } catch let error as NSError {
            log.error("failed to read DeviceData from Documents with error: \(error)")
            return nil
        }
    }

    private func documentsURLForDeviceData() -> URL? {
        guard let documentsDir = FileHelper().documentsDirPath() else { return nil }
        let filePath = (documentsDir as NSString).appendingPathComponent(documentsPath)
        return URL(fileURLWithPath: filePath)
    }

    // MARK: Keychain
    private func persistDeviceIdInKeychain(_ deviceId: String?) {
        LUKeychainAccess.standard().setString(deviceId, forKey: deviceIdKey)
    }

    private func retrieveDeviceIdFromKeychain() -> String? {
        return LUKeychainAccess.standard().string(forKey: deviceIdKey)
    }

    // MARK: Pasteboard
    private func persistDeviceIdInPasteboard(_ deviceId: String?) {
        pasteboard?.string = deviceId ?? ""
    }

    private func retrieveDeviceIdFromPasteboard() -> String? {
        if let deviceId = pasteboard?.string, !deviceId.isEmpty {
            return deviceId
        } else {
            return nil
        }
    }

    private lazy var pasteboard: UIPasteboard? = {
        let name = UIPasteboardName(rawValue: deviceIdKey)
        let pasteboard = UIPasteboard(name: name, create: true)
        pasteboard?.setPersistent(true)
        return pasteboard
    }()
}
