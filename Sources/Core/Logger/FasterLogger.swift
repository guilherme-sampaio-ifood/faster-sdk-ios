//
//  FasterLogger.swift
//  Faster
//
//  Created by Andre Alves on 3/26/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal let log = FasterLogger.shared

public final class FasterLogger {
    public static let shared = FasterLogger()

    public enum Level: Int {
        case debug = 0
        case info = 1
        case warning = 2
        case error = 3
    }

    public var formatter = FasterLogFormatter()
    public var currentLevel: Level = .warning

    /// log something which help during debugging (low priority)
    func debug(_ message: @autoclosure () -> Any, file: String = #file,
               function: String = #function, line: Int = #line, code: Int = 0) {
        custom(level: .debug, message: message, file: file, function: function, line: line, code: code)
    }

    /// log something which you are really interested but which is not an issue or error (normal priority)
    func info(_ message: @autoclosure () -> Any, file: String = #file,
              function: String = #function, line: Int = #line, code: Int = 0) {
        custom(level: .info, message: message, file: file, function: function, line: line, code: code)
    }

    /// log something which may cause big trouble soon (high priority)
    func warning(_ message: @autoclosure () -> Any, file: String = #file,
                 function: String = #function, line: Int = #line, code: Int = 0) {
        custom(level: .warning, message: message, file: file, function: function, line: line, code: code)
    }

    /// log something which will keep you awake at night (highest priority)
    func error(_ message: @autoclosure () -> Any, file: String = #file,
               function: String = #function, line: Int = #line, code: Int = 0) {
        custom(level: .error, message: message, file: file, function: function, line: line, code: code)
    }

    /// custom logging to manually adjust values, should just be used by other frameworks
    func custom(level: FasterLogger.Level, message: @autoclosure () -> Any,
                file: String = #file, function: String = #function, line: Int = #line, code: Int = 0) {

        if currentLevel.rawValue <= level.rawValue {
            let resolvedMessage = "\(message())"
            let formattedLog = "[Faster] \(formatter.format(level: level)) \(resolvedMessage)"
            NSLog(formattedLog)
        }
    }
}
