//
//  FasterLogFormatter.swift
//  Faster
//
//  Created by Andre Alves on 3/26/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

public class FasterLogFormatter {
    public struct LevelString {
        var debug = "[DEBUG]"
        var info = "ℹ️"
        var warning = "⚠️"
        var error = "🛑"
    }

    /*
     $D = start of date format
     $d = end of date format
     $L = log level
     $N = file name
     $F = function
     $I = line
     $M = message
     */
    public var format = "$DHH:mm:ss.SSS$d $L $N.$F:$I - $M"
    public var levelString = LevelString()

    public func format(level: FasterLogger.Level, message: String, file: String, function: String, line: Int, date: Date) -> String {
        var text = ""
        let phrases: [String] = format.components(separatedBy: "$")

        for phrase in phrases where !phrase.isEmpty {
            let firstChar = phrase[phrase.startIndex]
            let rangeAfterFirstChar = phrase.index(phrase.startIndex, offsetBy: 1)..<phrase.endIndex
            let remainingPhrase = phrase[rangeAfterFirstChar]

            switch firstChar {
            case "L":
                text += format(level: level) + remainingPhrase
            case "M":
                text += message + remainingPhrase
            case "N":
                text += format(fileName: file) + remainingPhrase
            case "F":
                text += function + remainingPhrase
            case "I":
                text += String(line) + remainingPhrase
            case "D":
                text += format(date: date, dateFormat: String(remainingPhrase))
            case "d":
                text += remainingPhrase
            default:
                text += phrase
            }
        }

        return text
    }

    public func format(level: FasterLogger.Level) -> String {
        switch level {
        case .debug:
            return levelString.debug
        case .info:
            return levelString.info
        case .warning:
            return levelString.warning
        case .error:
            return levelString.error
        }
    }

    public func format(fileName: String) -> String {
        let fileParts = fileName.components(separatedBy: "/")
        if let lastPart = fileParts.last {
            return lastPart
        }
        return ""
    }

    private let dateFormatter = DateFormatter()

    public func format(date: Date, dateFormat: String) -> String {
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
}
