//
//  FasterConfiguration.swift
//  Faster
//
//  Created by Andre Alves on 3/26/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

/**
 The `FasterConfiguration` contains many options for customization.

 To create the default configuration you shall use:
 ```
 FasterConfiguration.defaultConfiguration()
 ```
 */
public struct FasterConfiguration {
    /// The maximum amount of time (in seconds) that a session should be allowed to take in background. Default is 5 minutes.
    public var sessionTimeout: Int

    /// Tells the backend if it should create install events.
    /// This property can help you not create events for old users who installed the app before using Faster.
    /// By default, Faster tries to get Document's creationDate to decide if it's actually a new app install and not a app update.
    public var trackInstall: Bool

    public init(sessionTimeout: Int, trackInstall: Bool) {
        self.sessionTimeout = sessionTimeout
        self.trackInstall = trackInstall
    }
}

extension FasterConfiguration {
    public static func defaultConfiguration() -> FasterConfiguration {
        let trackInstall = FileHelper().isNewAppInstall()
        return FasterConfiguration(sessionTimeout: 60 * 5, trackInstall: trackInstall)
    }
}

private extension FileHelper {
    func isNewAppInstall() -> Bool {
        guard let path = documentsDirPath() else { return true }
        guard let attr = try? FileManager.default.attributesOfItem(atPath: path) else { return true }
        guard let creationDate = attr[.creationDate] as? Date else { return true }

        let days: TimeInterval = 2
        let interval: TimeInterval = days * 24 * 60 * 60
        return abs(creationDate.timeIntervalSinceNow) <= interval
    }
}
