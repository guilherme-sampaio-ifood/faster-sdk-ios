//
//  DimensionValue.swift
//  Faster
//
//  Created by Andre Alves on 3/27/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

public enum DimensionValue: Codable, Equatable, CustomStringConvertible {
    case string(String)
    case number(Decimal)
    case bool(Bool)
    case null

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        if let string = try? container.decode(String.self) {
            self = .string(string)
        } else if let bool = try? container.decode(Bool.self) {
            self = .bool(bool)
        } else if let number = try? container.decode(Decimal.self) {
            self = .number(number)
        } else {
            self = .null
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        switch self {
        case let .string(v): try container.encode(v)
        case let .number(v): try container.encode(v)
        case let .bool(v): try container.encode(v)
        case .null: try container.encodeNil()
        }
    }

    public func asAny() -> Any {
        switch self {
        case let .string(v): return v
        case let .number(v): return v
        case let .bool(v): return v
        case .null: return NSNull()
        }
    }

    public static func ==(lhs: DimensionValue, rhs: DimensionValue) -> Bool {
        switch(lhs, rhs) {
        case (.string(let l), .string(let r)): return l == r
        case (.number(let l), .number(let r)): return l == r
        case (.bool(let l), .bool(let r)): return l == r
        case (.null, .null): return true
        default: return false
        }
    }

    public var description: String {
        switch self {
        case let .string(v): return v
        case let .number(v): return v.description
        case let .bool(v): return v.description
        case .null: return "nil"
        }
    }
}
