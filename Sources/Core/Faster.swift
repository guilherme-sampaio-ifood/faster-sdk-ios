//
//  Faster.swift
//  Faster
//
//  Created by Andre Alves on 3/16/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

/// `Faster` ⚡️
public final class Faster {
    internal static let shared = Faster()

    private var core: FasterCore?

    /// The device data (installation).
    public static var deviceData: DeviceData? {
        return shared.core?.installer.deviceData
    }

    /// The current session.
    public static var session: Session? {
        return shared.core?.sessionController.session
    }

    /**
     Starts Faster using the application and secret key.

     It's recommended to call this method in AppDelegate's `didFinishLaunchingWithOptions`.

     **Call this method only once.**

     - parameter appKey: Your application key
     - parameter secretKey: Your application's secret key
     - parameter configuration: Options to start with.
     */
    public static func start(appKey: String, secretKey: String, configuration: FasterConfiguration = .defaultConfiguration()) {
        guard shared.core == nil else { return log.warning("start method already called!") }

        shared.core = FasterCore(appKey: appKey, secretKey: secretKey, configuration: configuration)
    }

    /// Forces the buffer to upload queued requests right now.
    public static func upload() {
        initializedCore { core in
            core.eventController.buffer.upload()
        }
    }

    /**
     Erase all data in disk and memory created by Faster.
     Since this method invalidates the Core object, you must call `start` again to continue using Faster.
     */
    public static func eraseAllData() {
        initializedCore { core in
            core.eraseAllData()
            Faster.shared.core = nil
        }
    }

    /**
     Adds geolocation information to the current session.

     The information should be added everytime the app opens and when significant changes in user's location happens.

     - parameter geolocation: The geolocation info. Use nil to remove.
     */
    public static func updateSession(geolocation: Session.Geolocation?) {
        initializedCore { core in
            core.sessionController.updateSession(geolocation: geolocation)
        }
    }

    /**
     Adds the userId to the current session.

     The information should be added everytime the app opens

     - parameter costumerId: The user unique identifier. Use nil to remove.
     */
    public static func updateSession(userId: String?) {
        initializedCore { core in
            core.sessionController.updateSession(userId: userId)
        }
    }

    /**
     Adds value for the key in Session's properties.

     The information should be added everytime the app opens

     - parameter value: The value. Use nil to remove.
     - parameter key: The key to use.
     */
    public static func updateSessionProperties(value: DimensionValue?, forKey key: String) {
        initializedCore { core in
            core.sessionController.updateSessionProperties(value: value, forKey: key)
        }
    }

    /**
     Adds value for the key in DeviceData's properties.

     - parameter value: The value. Use nil to remove.
     - parameter key: The key to use.
     */
    public static func updateDeviceProperties(value: DimensionValue?, forKey key: String) {
        initializedCore { core in
            core.installer.updateDeviceProperties(value: value, forKey: key)
        }
    }

    /**
     Updates DeviceData's pushToken.

     - parameter token: A token that identifies the device to APNs. Use nil to remove.
     */
    public static func updateRemoteNotifications(token: Data?) {
        initializedCore { core in
            core.installer.updateRemoteNotifications(token: token)
        }
    }

    /**
     Register a purchase event.

     - parameter price: The price.
     - parameter currency: The currency.
     - parameter productId: The productId.
     - parameter quantity: The quantity. Default is 1.
     - parameter externalId: A unique external identifier for the event, used for deduplication. Default is nil.
     */
    public static func registerPurchase(price: Double, currency: String, productId: String?, quantity: Int = 1, externalId: String? = nil) {
        let event = PurchaseEventBuilder.buildEvent(price: price, currency: currency,
                                                    productId: productId, quantity: quantity,
                                                    externalId: externalId)
        register(event: event)
    }

    /**
     Register event for the current session.

     For optimization purposes, events are not sent immediately. Whenever possible events will be dispatched in batches.

     - parameter event: The event to register.
     */
    public static func register(event: AppEvent) {
        initializedCore { core in
            core.eventController.sendRequest(forEvent: event)
        }
    }

    private static func initializedCore(completion: (FasterCore) -> Void) {
        guard let core = Faster.shared.core else { return log.error("missing call to start method!") }
        completion(core)
    }
}
