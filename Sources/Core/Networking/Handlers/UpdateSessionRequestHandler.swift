//
//  UpdateSessionRequestHandler.swift
//  Faster
//
//  Created by Andre Alves on 3/27/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

final class UpdateSessionRequestHandler: RequestHandler {

    var requestType: String {
        return RequestTypes.updateSession
    }

    var priority: RequestPriority {
        return .low
    }
}
