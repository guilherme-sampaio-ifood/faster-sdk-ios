//
//  CreateSessionRequestHandler.swift
//  Faster
//
//  Created by Andre Alves on 3/27/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

final class CreateSessionRequestHandler: RequestHandler {

    var requestType: String {
        return RequestTypes.createSession
    }

    var priority: RequestPriority {
        return .medium
    }
}
