//
//  CreateDeviceRequestHandler.swift
//  Faster
//
//  Created by Andre Alves on 3/25/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

final class CreateDeviceRequestHandler: RequestHandler {

    var requestType: String {
        return RequestTypes.createDevice
    }

    var priority: RequestPriority {
        return .veryHigh
    }

    func isRequestExpired(creationDate: Date) -> Bool {
        return false // never expire device creation requests
    }
}
