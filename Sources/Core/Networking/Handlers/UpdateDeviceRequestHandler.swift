//
//  UpdateDeviceRequestHandler.swift
//  Faster
//
//  Created by Andre Alves on 3/27/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

final class UpdateDeviceRequestHandler: RequestHandler {

    var requestType: String {
        return RequestTypes.updateDevice
    }

    var priority: RequestPriority {
        return .high
    }
}
