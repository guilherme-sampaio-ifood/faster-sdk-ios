//
//  TrackEventRequestHandler.swift
//  Faster
//
//  Created by Andre Alves on 3/27/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

final class TrackEventRequestHandler: RequestHandler {

    var requestType: String {
        return RequestTypes.trackEvent
    }

    var priority: RequestPriority {
        return .veryLow
    }
}
