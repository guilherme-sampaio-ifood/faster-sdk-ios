//
//  RequestOperation.swift
//  Faster
//
//  Created by Andre Alves on 3/23/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

enum RequestError: Error {
    case invalidResponseType
    case invalidStatusCode(Int)
    case unknownInternalError(NSError)
    case unknownRemoteError(NSError)

    var description: String {
        switch self {
        case .invalidResponseType:
            return "Invalid Response Type"
        case .invalidStatusCode(let code):
            return "Invalid Status Code (\(code))"
        case .unknownInternalError(let error):
            return "Unknown Internal Error \(error)"
        case .unknownRemoteError(let error):
            return "Unknown Remote Error \(error)"
        }
    }
}

enum RequestResult {
    case success(data: Data?, response: HTTPURLResponse)
    case failure(RequestError)
}

final class RequestOperation: AsynchronousOperation {
    let baseURL: URL
    let request: Request
    let urlSession: URLSession
    let hmacSerializer: FTHMACRequestSerializer

    private(set) var result: RequestResult?
    private(set) var dataTask: URLSessionDataTask?

    init(urlSession: URLSession, baseURL: URL, secret: String, request: Request) {
        self.urlSession = urlSession
        self.baseURL = baseURL
        self.hmacSerializer = FTHMACRequestSerializer(secret)
        self.request = request
        super.init()
    }

    override func execute() {
        var urlRequest: URLRequest

        do {
            urlRequest = try request.buildRequest(baseURL: baseURL)
        } catch let error as NSError {
            result = .failure(.unknownInternalError(error))
            return finish()
        }

        do {
            // This is very unlikely to fail but in case it does, we don't fail the request. Let the backend decides to accept it or not.
            urlRequest = try hmacSerializer.requestBySerializing(request: urlRequest, parameters: nil)
        } catch let error as NSError {
            log.error("failed to create signature for request of type \(request.requestType) with error \(error)")
        }

        guard !isCancelled else { return }

        dataTask = urlSession.dataTask(with: urlRequest) { [weak self] (data, response, error) in
            defer {
                self?.finish()
            }

            if let error = error as NSError? {
                self?.result = .failure(.unknownRemoteError(error))
                return
            }

            guard let response = response as? HTTPURLResponse else {
                self?.result = .failure(.invalidResponseType) // should never happen...
                return
            }

            let acceptableStatusCodes = 200..<300
            if acceptableStatusCodes.contains(response.statusCode) {
                self?.result = .success(data: data, response: response)
            } else {
                self?.result = .failure(.invalidStatusCode(response.statusCode))
            }
        }

        dataTask?.resume()
    }
}
