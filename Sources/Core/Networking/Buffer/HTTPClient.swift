//
//  HTTPClient.swift
//  Faster
//
//  Created by Andre Alves on 3/23/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class HTTPClient {
    internal let baseURL: URL
    internal let urlSession: URLSession
    internal let secret: String
    internal let operationQueue: OperationQueue
    internal let dispatchQueue: DispatchQueue

    private var reachability: Reachability?

    internal init(applicationKey: String, secret: String, baseURL: URL, dispatchQueue: DispatchQueue) {
        self.baseURL = baseURL
        self.secret = secret
        self.dispatchQueue = dispatchQueue

        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 180
        configuration.httpAdditionalHeaders = ["X-fstr-application-key" : applicationKey]
        self.urlSession = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)

        self.operationQueue = OperationQueue()
        self.operationQueue.underlyingQueue = dispatchQueue
        self.operationQueue.maxConcurrentOperationCount = 1

        startMonitoringNetwork()
    }

    deinit {
        stopMonitoringNetwork()
        urlSession.invalidateAndCancel()
    }

    internal func addOperation(request: Request, completion: @escaping (RequestResult) -> Void) {
        let operation = RequestOperation(urlSession: urlSession, baseURL: baseURL, secret: secret, request: request)

        operation.completionBlock = { [weak operation, weak self] in
            if let result = operation?.result {
                self?.dispatchQueue.async {
                    completion(result)
                }
            }
        }

        operationQueue.addOperation(operation)
    }

    internal func hasOperation(withRequest request: Request) -> Bool {
        let operations = operationQueue.operations.flatMap { $0 as? RequestOperation }
        return operations.contains(where: { $0.request.identifier == request.identifier })
    }

    internal func hasOperation(withRequestType requestType: String) -> Bool {
        let operations = operationQueue.operations.flatMap { $0 as? RequestOperation }
        return operations.contains(where: { $0.request.requestType == requestType })
    }

    internal func cancelAllOperations() {
        operationQueue.cancelAllOperations()
    }
}

// MARK: Reachability
extension HTTPClient {
    private func startMonitoringNetwork() {
        reachability = Reachability()
        try? reachability?.startNotifier()

        reachability?.whenReachable = { [weak self] _ in
            self?.dispatchQueue.async {
                self?.operationQueue.isSuspended = false
            }
        }

        reachability?.whenUnreachable = { [weak self] _ in
            self?.dispatchQueue.async {
                self?.operationQueue.isSuspended = true
            }
        }
    }

    private func stopMonitoringNetwork() {
        reachability?.stopNotifier()
        reachability = nil
    }
}
