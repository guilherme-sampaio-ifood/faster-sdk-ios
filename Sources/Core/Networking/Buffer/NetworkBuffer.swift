//
//  NetworkBuffer.swift
//  Faster
//
//  Created by Andre Alves on 3/16/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class NetworkBuffer {
    internal let applicationKey: String

    private let requestsPath: String
    /// The interval that determines when the requests are made.
    private let intervalToSync: TimeInterval = 15

    private let httpClient: HTTPClient
    private let dispatchQueue: DispatchQueue // Serial Queue

    private var handlers: [RequestHandler] = []
    private var syncTimer: SyncTimer?

    private var requestsToDeleteAfterSuccess: Set<String> = Set()

    internal init(applicationKey: String, secret: String) {
        self.applicationKey = applicationKey

        let appKey = FileHelper().sanitizeString(applicationKey)
        self.requestsPath = "Faster/\(appKey)/Requests"

        let queueName = String(format: "com.movile.faster.NetworkBuffer-%08x", arc4random())
        self.dispatchQueue = DispatchQueue(label: queueName)

        let baseURL = URL(string: "https://api.fstr.rocks/v1/")!
        self.httpClient = HTTPClient(applicationKey: applicationKey, secret: secret,
                                     baseURL: baseURL, dispatchQueue: dispatchQueue)

        self.syncTimer = SyncTimer(timeInterval: intervalToSync) { [weak self] in
            self?.dispatchQueue.async {
                self?.flush()
            }
        }

        // Initial flush
        dispatchQueue.asyncAfter(deadline: .now() + 5.0) { [weak self] in
            self?.flush()
        }
    }

    deinit {
        syncTimer?.invalidate()
        syncTimer = nil
    }

    internal func addHandler(_ handler: RequestHandler) {
        dispatchQueue.async {
            log.debug("[BUFFER] Adding RequestHandler of type \(handler.requestType) ")
            self.handlers.append(handler)
        }
    }

    internal func addRequest(_ request: Request) {
        dispatchQueue.async {
            guard let _ = self.handlers.first(where: { $0.requestType == request.requestType }) else {
                return log.error("[BUFFER] missing RequestHandler for request of type \(request.requestType)")
            }

            self.save(request: request)
        }
    }

    internal func upload() {
        dispatchQueue.async {
            self.flush()
        }
    }

    internal func removeAllRequests() {
        dispatchQueue.async {
            self.httpClient.cancelAllOperations()

            if let url = self.requestsDirectory() {
                do {
                    try FileHelper().deleteItemAtURLIfExists(url)
                } catch let error as NSError {
                    log.error("[BUFFER] failed to delete requests directory with error: \(error)")
                }
            }
        }
    }

    private func flush() {
        let handlers = self.handlers.sorted { $0.priority > $1.priority }
        var priorityToUpload: RequestPriority?

        for handler in handlers {
            if let priority = priorityToUpload, priority > handler.priority {
                break
            }

            let requests = requestsFromHandler(handler)

            if requests.count > 0 {
                priorityToUpload = handler.priority
            }

            for request in requests {
                guard !httpClient.hasOperation(withRequest: request) else { continue }

                log.info("[BUFFER] adding operation for request \(request.identifier) of type \(request.requestType)")

                requestsToDeleteAfterSuccess.insert(request.identifier)

                httpClient.addOperation(request: request) { [weak self] result in
                    switch result {
                    case let .success(_, response):
                        log.info("[BUFFER] request \(request.identifier) of type \(request.requestType) succeed with status code \(response.statusCode)")
                        if let `self` = self, self.requestsToDeleteAfterSuccess.contains(request.identifier) {
                            self.delete(request: request)
                        }

                    case let .failure(error):
                        switch error {
                        case .invalidStatusCode(let code) where code == 409:
                            // Error 409 is CONFLICT. In this case we just consider it as a success and delete the request.
                            if let `self` = self, self.requestsToDeleteAfterSuccess.contains(request.identifier) {
                                self.delete(request: request)
                            }

                        default:
                            log.error("[BUFFER] request of type \(request.requestType) failed with error: \(error)")
                        }
                    }

                    self?.requestsToDeleteAfterSuccess.remove(request.identifier)
                }
            }
        }
    }

    private func requestsFromHandler(_ handler: RequestHandler) -> [Request] {
        let fileHelper = FileHelper()

        guard let requestsDirectory = requestsDirectoryForRequestType(handler.requestType) else {
            log.error("[BUFFER] failed to get requests directory for handler of type \(handler.requestType)")
            return []
        }

        var requests: [Request] = []

        do {
            try fileHelper.createDirIfNeedAtURL(requestsDirectory)

            let files = try fileHelper.filesFromDirectoryURL(requestsDirectory).sorted { lhs, rhs in
                let lhsDate = fileHelper.creationDateForFile(lhs) ?? Date()
                let rhsDate = fileHelper.creationDateForFile(rhs) ?? Date()
                return lhsDate < rhsDate // serve old requests first
            }

            for file in files {
                do {
                    let data = try fileHelper.loadDataFromURL(file)

                    let tryExceptionBlock: () -> Void = {
                        if let request = NSKeyedUnarchiver.unarchiveObject(with: data) as? Request {
                            if let creationDate = fileHelper.creationDateForFile(file), handler.isRequestExpired(creationDate: creationDate) {
                                self.delete(request: request)

                            } else {
                                requests.append(request)
                            }

                        } else {
                            log.error("[BUFFER] unarchiving request of type \(handler.requestType) returned unexpected object")
                        }
                    }

                    let catchExceptionBlock: (NSException) -> Void = { exception in
                        log.error("[BUFFER] unarchiving request of type \(handler.requestType) raised exception: \(exception)")
                    }

                    FTExceptionHandler.try(tryExceptionBlock, catch: catchExceptionBlock, finally: nil)

                } catch let error as NSError {
                    log.error("[BUFFER] request data load from file \(file) failed with error: \(error)")
                }
            }

        } catch let error as NSError {
            log.error("[BUFFER] failed to get request files from directory \(requestsDirectory) with error: \(error)")
        }

        return requests
    }

    private func cleanRequestsForHandler(_ handler: RequestHandler) {
        log.debug("[BUFFER] cleaning requests for handler of type \(handler.requestType)")

        if let requestDirectory = requestsDirectoryForRequestType(handler.requestType) {
            do {
                try FileHelper().deleteItemAtURLIfExists(requestDirectory)
            } catch let error as NSError {
                log.error("[BUFFER] failed to clean requests of type \(handler.requestType) with error: \(error)")
            }
        }
    }

    private func save(request: Request) {
        log.debug("[BUFFER] saving request \(request.identifier) of type \(request.requestType)")

        guard let fileURL = fileURLForRequest(request) else {
            return log.error("[BUFFER] failed to get file url for request of type \(request.requestType)")
        }

        requestsToDeleteAfterSuccess.remove(request.identifier)

        let tryExceptionBlock: () -> Void = {
            do {
                // NSKeyedArchiver can raise OBJC exceptions
                // By doing the archive inside this closure, the exception is captured and does not crash the app
                let data = NSKeyedArchiver.archivedData(withRootObject: request)
                try FileHelper().saveData(data, destinationURL: fileURL)
            } catch let error as NSError {
                log.error("[BUFFER] save request of type \(request.requestType) failed with error: \(error)")
            }
        }

        let catchExceptionBlock: (NSException) -> Void = { exception in
            log.error("[BUFFER] save request of type \(request.requestType) raised exception: \(exception)")
        }

        FTExceptionHandler.try(tryExceptionBlock, catch: catchExceptionBlock, finally: nil)
    }

    private func delete(request: Request) {
        log.debug("[BUFFER] deleting request \(request.identifier) of type \(request.requestType)")

        if let fileURL = fileURLForRequest(request) {
            do {
                try FileHelper().deleteItemAtURLIfExists(fileURL)
            } catch let error as NSError {
                log.error("[BUFFER] request \(request.identifier) deletion failed with error: \(error)")
            }
        }
    }

    private func fileURLForRequest(_ request: Request) -> URL? {
        return requestsDirectoryForRequestType(request.requestType)?.appendingPathComponent("." + request.identifier)
    }

    private func requestsDirectoryForRequestType(_ requestType: String) -> URL? {
        return requestsDirectory()?.appendingPathComponent(requestType)
    }

    private func requestsDirectory() -> URL? {
        guard let documentsDir = FileHelper().documentsDirPath() else { return nil }
        return URL(fileURLWithPath: documentsDir).appendingPathComponent(requestsPath)
    }
}

private final class SyncTimer: NSObject {
    let timeInterval: TimeInterval
    let callback: () -> Void

    private var timer: Timer?

    init(timeInterval: TimeInterval, callback: @escaping () -> Void) {
        self.timeInterval = timeInterval
        self.callback = callback
        super.init()

        // timers can only be scheduled in the main queue
        DispatchQueue.main.async {
            self.scheduleTimer()
        }
    }

    private func scheduleTimer() {
        guard timer == nil else { return }

        timer = Timer.scheduledTimer(timeInterval: timeInterval,
                                     target: self,
                                     selector: #selector(tick),
                                     userInfo: nil,
                                     repeats: true)
    }

    // always remember to call this when finishes using this object
    func invalidate() {
        timer?.invalidate()
        timer = nil
    }

    @objc private func tick() {
        callback()
    }
}
