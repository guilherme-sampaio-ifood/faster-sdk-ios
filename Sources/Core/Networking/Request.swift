//
//  Request.swift
//  Faster
//
//  Created by Andre Alves on 3/23/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal struct RequestTypes {
    static let createDevice = "create_device"
    static let updateDevice = "update_device"
    static let createSession = "create_session"
    static let updateSession = "update_session"
    static let closeSession = "close_session"
    static let trackEvent = "track_event"
}

internal class Request: NSObject, NSCoding {
    private struct CodingKeys {
        static let identifierKey = "identifier"
        static let requestTypeKey = "requestType"
    }

    let identifier: String
    let requestType: String

    init(identifier: String = UUID().uuidString, requestType: String) {
        self.identifier = identifier
        self.requestType = requestType
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        guard let identifier = aDecoder.decodeObject(forKey: CodingKeys.identifierKey) as? String else {
            return nil
        }

        guard let requestType = aDecoder.decodeObject(forKey: CodingKeys.requestTypeKey) as? String else {
            return nil
        }

        self.identifier = identifier
        self.requestType = requestType
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(identifier, forKey: CodingKeys.identifierKey)
        aCoder.encode(requestType, forKey: CodingKeys.requestTypeKey)
    }

    func buildRequest(baseURL: URL) throws -> URLRequest {
        fatalError("Override buildRequest")
    }
}
