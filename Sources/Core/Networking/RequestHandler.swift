//
//  RequestHandler.swift
//  Faster
//
//  Created by Andre Alves on 3/23/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

enum RequestPriority: Int, Comparable {
    case veryHigh = 5
    case high = 4
    case medium = 3
    case low = 2
    case veryLow = 1

    static func <(lhs: RequestPriority, rhs: RequestPriority) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}

internal protocol RequestHandler {
    var requestType: String { get }
    var priority: RequestPriority { get }

    func isRequestExpired(creationDate: Date) -> Bool
}

extension RequestHandler {
    func isRequestExpired(creationDate: Date) -> Bool {
        let interval: TimeInterval = 60 * 60 * 24 * 7 * 2 // 2 weeks
        return abs(Date().timeIntervalSince(creationDate)) > interval
    }
}
