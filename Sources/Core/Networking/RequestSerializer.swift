//
//  RequestSerializer.swift
//  Faster
//
//  Created by Andre Alves on 3/25/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal protocol RequestSerializer {
    func requestBySerializing(request: URLRequest, parameters: Any?) throws -> URLRequest
}

internal final class JSONRequestSerializer: RequestSerializer {
    var writingOptions = JSONSerialization.WritingOptions(rawValue: 0)

    func requestBySerializing(request: URLRequest, parameters: Any?) throws -> URLRequest {
        var mutableRequest = request

        if let object = parameters {
            mutableRequest.httpBody = try JSONSerialization.data(withJSONObject: object, options: writingOptions)
            mutableRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            mutableRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        }

        return mutableRequest
    }
}

internal final class GzipRequestSerializer: RequestSerializer {
    /// The compression level is a floating point value between 0.0 and 1.0, with 0.0 meaning no compression and 1.0 meaning maximum compression.
    /// A value of 0.1 will provide the fastest possible compression.
    var compressionLevel: Float = 0.7

    enum Errors: Error {
        case gzipFailed
    }

    func requestBySerializing(request: URLRequest, parameters: Any?) throws -> URLRequest {
        var mutableRequest = request

        if let data = mutableRequest.httpBody as NSData? {
            if let gzippedData = data.ft_gzippedData(withCompressionLevel: compressionLevel) {
                mutableRequest.httpBody = gzippedData
                mutableRequest.setValue("gzip", forHTTPHeaderField: "Content-Encoding")
                mutableRequest.setValue("gzip", forHTTPHeaderField: "Accept-Encoding")
            } else {
                throw Errors.gzipFailed
            }
        }

        return mutableRequest
    }
}

// JSON -> -> GZIP
internal final class JSONGzipRequestSerializer: RequestSerializer {
    let jsonSerializer = JSONRequestSerializer()
    let gzipSerializer = GzipRequestSerializer()

    func requestBySerializing(request: URLRequest, parameters: Any?) throws -> URLRequest {
        let jsonRequest = try jsonSerializer.requestBySerializing(request: request, parameters: parameters)
        return try gzipSerializer.requestBySerializing(request: jsonRequest, parameters: nil)
    }
}

internal final class FTHMACRequestSerializer: RequestSerializer {
    let headerField = "X-fstr-signature"
    let secret: String

    enum Errors: Error {
        case failedToConvertSecretToData
        case failedToHMAC
    }

    init(_ secret: String) {
        self.secret = secret
    }

    func requestBySerializing(request: URLRequest, parameters: Any?) throws -> URLRequest {
        let objectsToHMAC: [Any?] = [
            request.httpMethod,
            request.url?.path,
            request.url?.query,
            request.httpBody,
            headerField
        ]

        guard let secretData = secret.data(using: .utf8) else {
            throw Errors.failedToConvertSecretToData
        }

        let signature = try objectsToHMAC.reduce(secretData, { accum, object -> Data in
            if let data = object as? Data, !data.isEmpty {
                if let result = FTHMAC.hmacData(data, secret: accum) {
                    return result
                } else {
                    throw Errors.failedToHMAC
                }
            } else {
                let string = (object as? String) ?? ""
                if let result = FTHMAC.hmacString(string, secret: accum) {
                    return result
                } else {
                    throw Errors.failedToHMAC
                }
            }
        })

        var mutableRequest = request
        mutableRequest.setValue(signature.base64EncodedString(), forHTTPHeaderField: headerField)
        return mutableRequest
    }
}
