//
//  UpdateDeviceRequest.swift
//  Faster
//
//  Created by Andre Alves on 3/27/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class UpdateDeviceRequest: Request {
    private let backendPath = "device"

    let requestParameters: [String: Any]

    init(deviceData: DeviceData) {
        requestParameters = deviceData.asDictionary()
        let identifier = "UPDATE-" + deviceData.id
        super.init(identifier: identifier, requestType: RequestTypes.updateDevice)
    }

    override func buildRequest(baseURL: URL) throws -> URLRequest {
        guard let deviceId = requestParameters[DeviceData.CodingKeys.id.stringValue] as? String else {
            throw NSError(domain: "faster.UpdateDeviceRequest.MissingDeviceId", code: 1, userInfo: nil)
        }

        let path = backendPath + "/" + deviceId
        var request = URLRequest(url: baseURL.appendingPathComponent(path))
        request.httpMethod = "PUT"

        var parameters = requestParameters
        parameters["sentAt"] = Date().timeIntervalSince1970

        let serializer = JSONGzipRequestSerializer()
        return try serializer.requestBySerializing(request: request, parameters: parameters)
    }

    private struct CodingKeys {
        static let requestParametersKey = "requestParameters"
    }

    required init?(coder aDecoder: NSCoder) {
        guard let requestParameters = aDecoder.decodeObject(forKey: CodingKeys.requestParametersKey) as? [String: Any] else {
            return nil
        }

        self.requestParameters = requestParameters
        super.init(coder: aDecoder)
    }

    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
        aCoder.encode(requestParameters, forKey: CodingKeys.requestParametersKey)
    }
}
