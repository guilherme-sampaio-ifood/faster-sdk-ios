//
//  TrackEventRequest.swift
//  Faster
//
//  Created by Andre Alves on 3/27/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class TrackEventRequest: Request {
    private let backendPath = "event"

    let requestParameters: [String: Any]

    init(deviceId: String, sessionId: String, event: AppEvent) {
        var parameters: [String: Any] = [:]
        parameters["deviceId"] = deviceId
        parameters["sessionId"] = sessionId
        parameters["events"] = [event.asDictionary()]
        requestParameters = parameters
        super.init(requestType: RequestTypes.trackEvent)
    }

    override func buildRequest(baseURL: URL) throws -> URLRequest {
        var request = URLRequest(url: baseURL.appendingPathComponent(backendPath))
        request.httpMethod = "POST"

        var parameters = requestParameters
        parameters["sentAt"] = Date().timeIntervalSince1970

        let serializer = JSONGzipRequestSerializer()
        return try serializer.requestBySerializing(request: request, parameters: parameters)
    }

    private struct CodingKeys {
        static let requestParametersKey = "requestParameters"
    }

    required init?(coder aDecoder: NSCoder) {
        guard let requestParameters = aDecoder.decodeObject(forKey: CodingKeys.requestParametersKey) as? [String: Any] else {
            return nil
        }

        self.requestParameters = requestParameters
        super.init(coder: aDecoder)
    }

    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
        aCoder.encode(requestParameters, forKey: CodingKeys.requestParametersKey)
    }
}
