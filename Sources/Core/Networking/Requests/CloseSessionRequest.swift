//
//  CloseSessionRequest.swift
//  Faster
//
//  Created by Andre Alves on 3/27/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class CloseSessionRequest: Request {
    private let backendPath = "session"

    let requestParameters: [String: Any]

    init(session: Session) {
        var parameters: [String: Any] = [:]
        parameters[Session.CodingKeys.id.stringValue] = session.id
        parameters["localTimestamp"] = session.closeTimestamp
        requestParameters = parameters

        super.init(requestType: RequestTypes.closeSession)
    }

    override func buildRequest(baseURL: URL) throws -> URLRequest {
        guard let sessionId = requestParameters[Session.CodingKeys.id.stringValue] as? String else {
            throw NSError(domain: "faster.CloseSessionRequest.MissingSessionId", code: 1, userInfo: nil)
        }

        let path = backendPath + "/" + sessionId + "/" + "close"
        var request = URLRequest(url: baseURL.appendingPathComponent(path))
        request.httpMethod = "POST"

        var parameters = requestParameters
        parameters["sentAt"] = Date().timeIntervalSince1970

        let serializer = JSONGzipRequestSerializer()
        return try serializer.requestBySerializing(request: request, parameters: parameters)
    }

    private struct CodingKeys {
        static let requestParametersKey = "requestParameters"
    }

    required init?(coder aDecoder: NSCoder) {
        guard let requestParameters = aDecoder.decodeObject(forKey: CodingKeys.requestParametersKey) as? [String: Any] else {
            return nil
        }

        self.requestParameters = requestParameters
        super.init(coder: aDecoder)
    }

    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
        aCoder.encode(requestParameters, forKey: CodingKeys.requestParametersKey)
    }
}
