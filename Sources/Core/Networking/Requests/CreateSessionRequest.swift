//
//  CreateSessionRequest.swift
//  Faster
//
//  Created by Andre Alves on 3/27/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class CreateSessionRequest: Request {
    private let backendPath = "session"

    let requestParameters: [String: Any]

    init(session: Session) {
        requestParameters = session.asDictionary()
        super.init(requestType: RequestTypes.createSession)
    }

    override func buildRequest(baseURL: URL) throws -> URLRequest {
        var request = URLRequest(url: baseURL.appendingPathComponent(backendPath))
        request.httpMethod = "POST"

        var parameters = requestParameters
        parameters["sentAt"] = Date().timeIntervalSince1970

        let serializer = JSONGzipRequestSerializer()
        return try serializer.requestBySerializing(request: request, parameters: parameters)
    }

    private struct CodingKeys {
        static let requestParametersKey = "requestParameters"
    }

    required init?(coder aDecoder: NSCoder) {
        guard let requestParameters = aDecoder.decodeObject(forKey: CodingKeys.requestParametersKey) as? [String: Any] else {
            return nil
        }

        self.requestParameters = requestParameters
        super.init(coder: aDecoder)
    }

    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
        aCoder.encode(requestParameters, forKey: CodingKeys.requestParametersKey)
    }
}
