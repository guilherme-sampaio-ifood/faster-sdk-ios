//
//  CreateDeviceRequest.swift
//  Faster
//
//  Created by Andre Alves on 3/25/18.
//  Copyright © 2018 Movile. All rights reserved.
//

import Foundation

internal final class CreateDeviceRequest: Request {
    private let backendPath = "device"

    let requestParameters: [String: Any]

    init(deviceData: DeviceData, session: Session, isReinstall: Bool, trackInstall: Bool) {
        let deviceDictionary = deviceData.asDictionary()
        let sessionDictionary = session.asDictionary()
        var parameters = deviceDictionary.merging(sessionDictionary) { (current, _) in current }
        parameters["trackInstall"] = trackInstall

        if isReinstall {
            parameters["reinstall"] = true
        }

        requestParameters = parameters
        super.init(requestType: RequestTypes.createDevice)
    }

    override func buildRequest(baseURL: URL) throws -> URLRequest {
        var request = URLRequest(url: baseURL.appendingPathComponent(backendPath))
        request.httpMethod = "POST"

        var parameters = requestParameters
        parameters["sentAt"] = Date().timeIntervalSince1970

        let serializer = JSONGzipRequestSerializer()
        return try serializer.requestBySerializing(request: request, parameters: parameters)
    }

    private struct CodingKeys {
        static let requestParametersKey = "requestParameters"
    }

    required init?(coder aDecoder: NSCoder) {
        guard let requestParameters = aDecoder.decodeObject(forKey: CodingKeys.requestParametersKey) as? [String: Any] else {
            return nil
        }

        self.requestParameters = requestParameters
        super.init(coder: aDecoder)
    }

    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
        aCoder.encode(requestParameters, forKey: CodingKeys.requestParametersKey)
    }
}
