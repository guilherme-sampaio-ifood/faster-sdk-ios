//
//  FTHMAC.m
//  Faster
//
//  Created by Andre Alves on 3/27/18.
//  Copyright © 2018 Movile. All rights reserved.
//

#import "FTHMAC.h"
#include <CommonCrypto/CommonHMAC.h>

@implementation FTHMAC

+ (nullable NSData *)hmacString:(nonnull NSString *)string secret:(nonnull NSData *)secret {
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    return [FTHMAC hmacData:data secret:secret];
}

+ (nullable NSData *)hmacData:(nonnull NSData *)data secret:(nonnull NSData *)secret {
    if (!data || !secret) {
        return nil;
    }

    NSMutableData *hMacOut = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, secret.bytes, secret.length, data.bytes, data.length, hMacOut.mutableBytes);
    return hMacOut;
}

@end
