//
//  FTHMAC.h
//  Faster
//
//  Created by Andre Alves on 3/27/18.
//  Copyright © 2018 Movile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTHMAC : NSObject

+ (nullable NSData *)hmacString:(nonnull NSString *)string secret:(nonnull NSData *)secret;
+ (nullable NSData *)hmacData:(nonnull NSData *)data secret:(nonnull NSData *)secret;

@end
