//
//  FTExceptionHandler.m
//  Faster
//
//  Created by Andre Alves on 3/21/18.
//  Copyright © 2018 Movile. All rights reserved.
//

#import "FTExceptionHandler.h"

@implementation FTExceptionHandler

+ (void)tryBlock:(nonnull __attribute__((noescape)) void(^)(void))tryBlock {
    [FTExceptionHandler tryBlock:tryBlock catchBlock:nil finally:nil];
}

+ (void)tryBlock:(nonnull __attribute__((noescape)) void(^)(void))tryBlock catchBlock:(nullable __attribute__((noescape)) void(^)(NSException* _Nonnull))catchBlock finally:(nullable __attribute__((noescape)) void(^)(void))finallyBlock {
    NSAssert(tryBlock != NULL, @"try block cannot be null");
    @try {
        tryBlock();
    }
    @catch (NSException *ex) {
        if(catchBlock != NULL) {
            catchBlock(ex);
        }
    }
    @finally {
        if(finallyBlock != NULL) {
            finallyBlock();
        }
    }
}

@end
