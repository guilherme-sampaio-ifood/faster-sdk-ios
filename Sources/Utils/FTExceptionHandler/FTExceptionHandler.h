//
//  FTExceptionHandler.h
//  Faster
//
//  Created by Andre Alves on 3/21/18.
//  Copyright © 2018 Movile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTExceptionHandler : NSObject

+ (void)tryBlock:(nonnull __attribute__((noescape)) void(^)(void))tryBlock;

+ (void)tryBlock:(nonnull __attribute__((noescape)) void(^)(void))tryBlock
      catchBlock:(nullable __attribute__((noescape)) void(^)(NSException* _Nonnull))catchBlock
         finally:(nullable __attribute__((noescape)) void(^)(void))finallyBlock;

@end
